$(document).ready(function () {
    function showTheTime() {
        $('#time').html(moment().zone(-4).format('HH:mm:ss'));
    }

    function showMenu() {
        var sidebar = $('.sidebar');
        sidebar.show();
        //   sidebar.show('slide', { to: { width: 300, height: 100 } }, 0, function () {
        var main = $('.main');
        main.addClass('col-sm-9');
        main.addClass('col-sm-offset-3');
        main.addClass('col-md-10');
        main.addClass('col-md-offset-2');
        main.removeClass('col-md-12');
        //   });
    }

    function hideMenu() {
        var sidebar = $('.sidebar');
        sidebar.hide();
        //    sidebar.hide('slide', { to: { width: 300, height: 100 } }, 0, function () {
        var main = $('.main');
        main.removeClass('col-sm-9');
        main.removeClass('col-sm-offset-3');
        main.removeClass('col-md-10');
        main.removeClass('col-md-offset-2');
        main.addClass('col-md-12');
        //   });
    }

    var visibleMenu = $.cookie('menu');
    if (visibleMenu == undefined) {
        visibleMenu = 1;
        $.cookie('menu', 1);
    }
    if (visibleMenu == 1) {
        showMenu();
    } else {
        hideMenu();
    }
    showTheTime();
    setInterval(showTheTime, 250);

    $('.alert').show();
    $('#error-alert').bind('closed.bs.alert', function (e) {
        e.preventDefault();
        $('.form-group').removeClass('has-error');
        $('.text-danger').hide('blind', {}, 1000, function () {
            $('.form-group').find('.text-danger').remove();
        });

    });
    $('.show-hide-menu').on('click', function (e) {
        e.preventDefault();
        if ($('.sidebar').is(":visible")) {
            $.cookie('menu', 0);
            hideMenu();

        } else {
            $.cookie('menu', 1);
            showMenu();
        }
    });
});