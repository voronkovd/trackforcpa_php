<?php

return array(
    'widgets' => array(
        'CJuiDatePicker' => array(
            'language' => 'ru',
            'options' => array(
                'changeMonth' => true,
                'changeYear' => true,
                'dateFormat' => 'yy-mm-dd',
                'showOn' => 'focus',
                'showOtherMonths' => true,
                'selectOtherMonths' => true,
                'yearRange' => '2014:' . date('Y')
            ),
            'htmlOptions' => array()
        ),
        'CGridView' => array(
            'template' => "{summary}{items}\n{pager}",
            'cssFile' => '/static/css/griedview.css',
            'summaryText' => 'Всего: <span>{count}</span>',
            'pagerCssClass' => 'pager-div',
            'ajaxUpdate' => false,
            'pager' => array(
                'class' => 'CLinkPager',
                'cssFile' => '/static/css/pager.css',
                'header' => '',
                'selectedPageCssClass' => 'active',
                'nextPageLabel' => '',
                'prevPageLabel' => '',
                'firstPageLabel' => '',
                'lastPageLabel' => '',
            ),
        ),
        'CDetailView' => array(
            'cssFile' => '/static/css/detailView.css',
        ),
    ),
);