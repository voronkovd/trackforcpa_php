<?php
return array(
    'connectionString' => 'mysql:host=localhost;dbname=get_parts',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8',
    'schemaCachingDuration' => 3600,
);