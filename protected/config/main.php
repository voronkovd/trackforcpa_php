<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Get Parts',
    'preload' => array('log'),
    'language' => 'ru',
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        'admin',
        'users',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    'components' => array(
        'redis' => array(
            'class' => 'application.components.RedisConnection',
            'options' => array(
                'servers' => array(
                    'server1' => array(
                        'host' => 'localhost',
                        'port' => '6379',
                        'timeout' => '3',
                        'readTimeout' => '3',
                    ),
                ),
                'serializerAdapter' => 'json',
            ),
        ),
        'request' => array(
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
        ),
        'mailer' => array(
            'class' => 'ext.mailer.SmtpMailer',
            'server' => 'smtp.163.com',
            'port' => '25',
            'username' => 'your username',
            'password' => 'your password',
            'class' => 'ext.mailer.PhpMailer',
        ),
        'cache' => include_once('cache.php'),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
            'loginUrl' => array('/users/auth/login'),
        ),
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => '/static/js/jquery-1.11.0.min.js',
                'jquery.min.js' => '/static/js/jquery-1.11.0.min.js',
                'jquery-ui.js' => '/static/js/jquery-ui.min.js',
                'jquery-ui.min.js' => '/static/js/jquery-ui.min.js',
            ),
        ),
        'widgetFactory' => include_once('widgets.php'),
        'urlManager' => include_once('url_manager.php'),
        'db' => include_once('db.php'),
        'errorHandler' => array('errorAction' => 'site/error'),
        'log' => include_once('log.php'),
    ),
    'params' => include_once('params.php'),
);