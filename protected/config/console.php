<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Track For CPA Console',
    'import' => array('application.models.*', 'application.components.*'),
    'components' => array('db' => include_once('db.php')),
    'params' => include_once('params.php')
);