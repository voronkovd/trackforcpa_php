<?php
return array(
    'class' => 'CLogRouter',
    'routes' => array(
        array(
            'class' => 'CFileLogRoute',
            'levels' => 'profile',
            'categories' => 'system.*'
        ),
        array(
            'class' => 'CProfileLogRoute',
            'levels' => 'profile',
            'enabled' => false,
        ),
    ),
);