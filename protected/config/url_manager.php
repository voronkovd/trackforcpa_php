<?php

return array(
    'urlFormat' => 'path',
    'showScriptName' => false,
    'rules' => array(
        'login' => 'users/auth/login',
        'logout' => 'users/auth/logout',
        'change/password' => 'users/user/changePassword',
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

    ),
);