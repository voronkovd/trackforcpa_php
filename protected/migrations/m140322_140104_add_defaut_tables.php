<?php

class m140322_140104_add_defaut_tables extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            "users",
            array(
                "id" => "pk",
                "email" => "VARCHAR(32) NOT NULL COMMENT 'Email'",
                "password" => "VARCHAR(32) NOT NULL COMMENT 'Пароль'",
                "role" => "TINYINT(1) DEFAULT 1 COMMENT 'Роль'",
                "last_login" => "DATETIME NOT NULL COMMENT 'Последнее посещение'",
                "status" => "TINYINT(1) DEFAULT 1 COMMENT 'Статус'"
            )
        );

        $this->createTable(
            "categories",
            array(
                "id" => "pk",
                "user_id" => "INT(11) NOT NULL COMMENT 'Пользоатель'",
                "name" => "VARCHAR(32) NOT NULL COMMENT 'Наименование'"
            )
        );
        $this->createTable(
            "links",
            array(
                "id" => "pk",
                "user_id" => "INT(11) NOT NULL COMMENT 'Пользователь'",
                "name" => "VARCHAR(32) NOT NULL COMMENT 'Наименование'",
                "url" => "VARCHAR(253) NOT NULL COMMENT 'Ссылка'"
            )
        );

        $this->createTable(
            "rules",
            array(
                "id" => "pk",
                "category_id" => "INT(11) NOT NULL COMMENT 'Категория'",
                "link_id" => "INT(11) NOT NULL COMMENT 'Ссылка'",
                "name" => "VARCHAR(32) NOT NULL COMMENT 'Наименование'"
            )
        );
        $this->createTable(
            "refers",
            array(
                "id" => "pk",
                "user_id" => "INT(11) NOT NULL COMMENT 'Пользователь'",
                "url" => "VARCHAR(253) NOT NULL COMMENT 'Ссылка'"
            )
        );
        $this->createTable(
            "events",
            array(
                "id" => "pk",
                "rule_id" => "INT(11) NOT NULL COMMENT 'Категория'",
                "link_id" => "INT(11) NOT NULL COMMENT 'Ссылка'",
                "type" => "TINYINT(1) NOT NULL COMMENT 'Тип события'",
                "value" => "INT(11) NOT NULL COMMENT 'Значение'"
            )
        );
        $this->createTable(
            "clicks",
            array(
                "id" => "pk",
                "rule_id" => "INT(11) NOT NULL COMMENT 'Правило'",
                "link_id" => "INT(11) NOT NULL COMMENT 'Ссылка'",
                "refer_id" => "INT(11) NOT NULL COMMENT 'Сайт'",
                "user_id" => "INT(11) NOT NULL COMMENT 'Пользователь'",
                "geo_ip_ip_range_id" => "INT(11) DEFAULT NULL COMMENT 'Местоположения'",
                "ip" => "VARCHAR(15) DEFAULT '0.0.0.0' COMMENT 'IP адрес'",
                "os" => "TINYINT(1) NOT NULL COMMENT 'Операционная система'",
                "browser" => "TINYINT(1) NOT NULL COMMENT 'Браузер'",
                "created" => "DATETIME NOT NULL COMMENT 'Дата и время'",
                "order" => "TINYINT(1) DEFAULT 0 COMMENT 'Покупка товара'",
            )
        );
        $this->createTable(
            "imports",
            array(
                "id" => "pk",
                "link_id" => "INT(11) NOT NULL COMMENT 'Ссылка'",
                "user_id" => "INT(11) NOT NULL COMMENT 'Пользователь'",
                "ip" => "VARCHAR(15) DEFAULT '0.0.0.0' COMMENT 'IP адрес'",
                "created" => "DATETIME NOT NULL COMMENT 'Дата и время'",
            )
        );

    }

    public function down()
    {
        $this->dropTable('imports');
        $this->dropTable('clicks');
        $this->dropTable('refers');
        $this->dropTable('events');
        $this->dropTable('rules');
        $this->dropTable('links');
        $this->dropTable('categories');
        $this->dropTable('users');
    }

}