<?php

class m140409_103723_add_browser_os_tables extends CDbMigration
{
    public function up()
    {
        $this->createTable("browsers", array("id" => "pk", "name" => "VARCHAR(32) NOT NULL COMMENT 'браузер'"));
        $this->createTable("oses", array("id" => "pk", "name" => "VARCHAR(32) NOT NULL COMMENT 'ОС'"));
        $this->dropColumn('clicks', 'browser');
        $this->dropColumn('clicks', 'os');
        $this->addColumn('clicks', 'browser_id', 'INT(11) DEFAULT NULL');
        $this->addColumn('clicks', 'os_id', 'INT(11) NOT NULL');
        $this->addColumn('clicks', 'device_type', 'TINYINT(1) NOT NULL');
        $this->addColumn('clicks', 'is_bot', 'VARCHAR(32) DEFAULT NULL');
        $this->createIndex('index_clicks_browser', 'clicks', 'browser_id');
        $this->createIndex('index_clicks_os', 'clicks', 'os_id');
        $this->addForeignKey('key_clicks_browser', 'clicks', 'browser_id', 'browsers', 'id');
        $this->addForeignKey('key_clicks_os', 'clicks', 'os_id', 'oses', 'id');
    }

    public function down()
    {
    }

}