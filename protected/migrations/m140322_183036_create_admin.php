<?php

class m140322_183036_create_admin extends CDbMigration
{
    public function up()
    {
        $this->insert(
            'users',
            array(
                "email" => "admin@trackforcpa.ru",
                "password" => md5(Yii::app()->params['secret_key'] . 'cv1234h'),
                "role" => 2,
                "last_login" => date('Y-m-d H:i:s')
            )
        );
    }

    public function down()
    {
        $this->delete('users', array('id' => 1));
    }
}