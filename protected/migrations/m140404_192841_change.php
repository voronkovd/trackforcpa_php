<?php

class m140404_192841_change extends CDbMigration
{
    public function up()
    {
        $this->dropColumn('users', 'role');
        $this->addColumn('users', 'activation', 'VARCHAR(32) DEFAULT NULL');
        $this->addColumn('users', 'date_registration', "DATETIME NOT NULL COMMENT 'Дата регистрации'");
        $this->addColumn('rules', 'token', 'VARCHAR(32) NOT NULL');
        $this->addColumn('rules', 'status', 'TINYINT(1) DEFAULT 1');
    }

    public function down()
    {

    }
}