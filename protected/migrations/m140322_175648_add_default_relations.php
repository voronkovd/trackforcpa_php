<?php

class m140322_175648_add_default_relations extends CDbMigration
{
    public function up()
    {
        $this->createIndex('index_categories_user', 'categories', 'user_id');
        $this->createIndex('index_links_user', 'links', 'user_id');
        $this->createIndex('index_rules_category', 'rules', 'category_id');
        $this->createIndex('index_rules_link', 'rules', 'link_id');
        $this->createIndex('index_refers_user', 'refers', 'user_id');
        $this->createIndex('index_events_rule', 'events', 'rule_id');
        $this->createIndex('index_events_link', 'events', 'link_id');
        $this->createIndex('index_rules_link', 'clicks', 'link_id');
        $this->createIndex('index_refers_user', 'clicks', 'user_id');
        $this->createIndex('index_refers_rule', 'clicks', 'rule_id');
        $this->createIndex('index_refers_refer', 'clicks', 'refer_id');
        $this->createIndex('index_imports_link', 'imports', 'link_id');
        $this->createIndex('index_imports_user', 'imports', 'user_id');
        $this->addForeignKey('key_categories_user', 'categories', 'user_id', 'users', 'id');
        $this->addForeignKey('key_links_user', 'links', 'user_id', 'users', 'id');
        $this->addForeignKey('key_rules_category', 'rules', 'category_id', 'categories', 'id');
        $this->addForeignKey('key_rules_link', 'rules', 'link_id', 'links', 'id');
        $this->addForeignKey('key_refers_user', 'refers', 'user_id', 'users', 'id');
        $this->addForeignKey('key_events_rule', 'events', 'rule_id', 'rules', 'id');
        $this->addForeignKey('key_events_link', 'events', 'link_id', 'links', 'id');
        $this->addForeignKey('key_clicks_user', 'clicks', 'user_id', 'users', 'id');
        $this->addForeignKey('key_clicks_rule', 'clicks', 'rule_id', 'rules', 'id');
        $this->addForeignKey('key_clicks_link', 'clicks', 'link_id', 'links', 'id');
        $this->addForeignKey('key_clicks_refer', 'clicks', 'refer_id', 'refers', 'id');
        $this->addForeignKey('key_imports_link', 'imports', 'link_id', 'links', 'id');
        $this->addForeignKey('key_imports_user', 'imports', 'user_id', 'users', 'id');


    }

    public function down()
    {

        $this->dropForeignKey('key_categories_user', 'categories');
        $this->dropForeignKey('key_links_user', 'links');
        $this->dropForeignKey('key_rules_category', 'rules');
        $this->dropForeignKey('key_rules_link', 'rules');
        $this->dropForeignKey('key_refers_user', 'refers');
        $this->dropForeignKey('key_events_rule', 'events');
        $this->dropForeignKey('key_events_link', 'events');
        $this->dropForeignKey('key_clicks_user', 'clicks');
        $this->dropForeignKey('key_clicks_rule', 'clicks');
        $this->dropForeignKey('key_clicks_link', 'clicks');
        $this->dropForeignKey('key_clicks_refer', 'clicks');
        $this->dropForeignKey('key_imports_link', 'imports');
        $this->dropForeignKey('key_imports_user', 'imports');
        $this->dropIndex('index_categories_user', 'categories');
        $this->dropIndex('index_links_user', 'links');
        $this->dropIndex('index_rules_category', 'rules');
        $this->dropIndex('index_rules_link', 'rules');
        $this->dropIndex('index_refers_user', 'refers');
        $this->dropIndex('index_events_rule', 'events');
        $this->dropIndex('index_events_link', 'events');
        $this->dropIndex('index_clicks_user', 'clicks');
        $this->dropIndex('index_clicks_rule', 'clicks');
        $this->dropIndex('index_clicks_link', 'clicks');
        $this->dropIndex('index_clicks_refer', 'clicks');
        $this->dropIndex('index_imports_link', 'imports');
        $this->dropIndex('index_imports_user', 'imports');
    }
}