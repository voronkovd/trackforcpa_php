<?php

class City extends ActiveRecord
{
    public function tableName()
    {
        return 'django_geoip_city';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array(
            'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
            'ip_ranges' => array(self::HAS_MANY, 'IpRange', 'city_id'),
        );
    }

    public function attributeLabels()
    {
        return array();
    }

    public function search()
    {
        return new CActiveDataProvider($this);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}