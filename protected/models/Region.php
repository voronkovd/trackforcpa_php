<?php

class Region extends ActiveRecord
{
    public function tableName()
    {
        return 'django_geoip_region';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array(
            'Cities' => array(self::HAS_MANY, 'City', 'region_id'),
            'range' => array(self::HAS_MANY, 'Iprange', 'region_id'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
        );
    }

    public function attributeLabels()
    {
        return array();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}