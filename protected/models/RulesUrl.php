<?php

class RulesUrl extends ActiveRecord
{
    public $categories = array();

    public $links = array();

    public static $statuses = array(1 => 'В работе', 0 => 'Не в работе');

    public $rulesOptions
        = array(
            'name' => array(
                'maxlength' => 32,
                'autocomplete' => 'off',
                'autofocus' => 'autofocus',
                'placeholder' => 'Например: Правило #1'

            )
        );

    public function tableName()
    {
        return 'rules';
    }

    public function rules()
    {
        return array(
            array('category_id, link_id, name', 'required', 'message' => 'Поле не может быть пустым'),
            array('name', 'length', 'min' => 2, 'tooShort' => 'Минимальная длина 2 символа'),
            array('name', 'length', 'max' => 32, 'tooLong' => 'Максимальная длина 32 символа'),
            array('status', 'boolean'),
            array('id, category_id, link_id, name, token, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'clicks' => array(self::HAS_MANY, 'Clicks', 'link_id'),
            'events' => array(self::HAS_MANY, 'Events', 'link_id'),
            'link' => array(self::BELONGS_TO, 'Links', 'link_id'),
            'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'category_id' => 'Категория',
            'link_id' => 'Ссылка',
            'name' => 'Наименование',
            'token' => 'Сгенерированная ссылка',
            'status' => 'Статус'
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->with = array(
            'category' => array(
                'select' => 'category.name',
                'joinType' => 'RIGHT JOIN',
                'condition' => 'category.user_id=' . Yii::app()->user->id
            ),
            'link' => array('select' => 'link.name')
        );
        $criteria->compare('t.category_id', $this->category_id);
        $criteria->compare('t.link_id', $this->link_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.status', $this->status);
        $criteria->select = 't.id, t.category_id, t.name, t.token, t.status';
        $criteria->order = 't.id DESC';
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM rules');
        $model = self::model()->cache(self::$duration, $dep, 1);

        return new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => $this->pagination));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        $with = array(
            'category' => array(
                'select' => 'category.name',
                'joinType' => 'RIGHT JOIN',
                'condition' => 'category.user_id=' . Yii::app()->user->id
            )
        );

        return array(
            'select' => array('select' => 't.id, t.name', 'with' => $with),
            'self' => array('select' => 't.id, t.category_id, t.link_id, t.name, t.token, t.status', 'with' => $with),
            'countSelf' => array('select' => 'COUNT(t.id) as id', 'with' => $with)
        );
    }

    public function beforeDelete()
    {

        $criteria = new CDbCriteria();
        $criteria->compare('rule_id', $this->id);
        Clicks::model()->deleteAll($criteria);
        Events::model()->deleteAll($criteria);

        return true;
    }

    public function afterSave()
    {
        $data = array(
            'rule' => array(
                'id' => $this->id,
                'token' => $this->token,
                'status' => $this->status,
                'link' => array(
                    'id' => $this->link->id,
                    'url' => $this->link->url,
                    'user_id' => $this->link->user_id
                ),
                'events' => array(),
            )
        );
        if (!empty($this->events)) {
            foreach ($this->events as $event) {
                $data['rule']['events'][] = array();
            }
        }
        Yii::app()->redis->getConnection()->set($this->token, $data);
    }

    public function urlEncode()
    {
        $url = Yii::app()->params['service'] . $this->id . '/' . $this->token;

        return CHtml::link($url, $url, array('target' => '_blank'));
    }

    public function getStatus()
    {
        if ($this->status == 1) {
            $class = 'glyphicon glyphicon-ok';
        } else {
            $class = 'glyphicon glyphicon-remove';
        }

        return CHtml::tag('span', array('class' => $class));
    }

    public static function  getDropDown()
    {
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM rules');
        $model = self::model()->cache(self::$duration, $dep, 1)->select()->findAll();

        return CHtml::listData($model, 'id', 'name');
    }
}