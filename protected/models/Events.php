<?php

class Events extends ActiveRecord
{
    public function tableName()
    {
        return 'events';
    }

    public function rules()
    {
        return array(
            array('rule_id, link_id, type, value', 'required'),
            array('rule_id, link_id, type, value', 'numerical', 'integerOnly' => true),
            array('id, rule_id, link_id, type, value', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'link' => array(self::BELONGS_TO, 'RulesUrl', 'link_id'),
            'rule' => array(self::BELONGS_TO, 'Links', 'rule_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'rule_id' => 'Rule',
            'link_id' => 'Link',
            'type' => 'Type',
            'value' => 'Value',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 10)));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        return array('public' => array('select' => '*', 'condition' => array()));
    }

}