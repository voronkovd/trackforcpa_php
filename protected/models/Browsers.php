<?php

class Browsers extends ActiveRecord
{
    public function tableName()
    {
        return 'browsers';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array('clicks' => array(self::HAS_MANY, 'Clicks', 'browser_id'));
    }

    public function attributeLabels()
    {
        return array();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        return array('select' => array('select' => 't.id, t.name'));
    }

    public static function  getDropDown()
    {
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM browsers');
        $model = self::model()->cache(self::$duration, $dep, 1)->select()->findAll();

        return CHtml::listData($model, 'id', 'name');
    }

}