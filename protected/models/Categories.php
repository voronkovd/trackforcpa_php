<?php

class Categories extends ActiveRecord
{

    public $rulesOptions
        = array(
            'name' => array(
                'maxlength' => 32,
                'autocomplete' => 'off',
                'autofocus' => 'autofocus',
                'placeholder' => 'Например: ЦПА система №1'

            )
        );

    public function tableName()
    {
        return 'categories';
    }

    public function rules()
    {
        return array(
            array('name', 'required', 'message' => 'Поле не может быть пустым'),
            array('name', 'length', 'min' => 2, 'tooShort' => 'Минимальная длина 2 символа'),
            array('name', 'length', 'max' => 32, 'tooLong' => 'Максимальная длина 32 символа'),
            array('id, user_id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rules' => array(self::HAS_MANY, 'RulesUrl', 'category_id'),
        );
    }

    public function attributeLabels()
    {
        return array('name' => 'Наименование');
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, t.user_id, t.name';
        $criteria->compare('t.user_id', Yii::app()->user->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->order = 't.id DESC';
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM categories');
        $model = self::model()->cache(self::$duration, $dep, 1);

        return new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => $this->pagination));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        $condition = 'user_id=' . Yii::app()->user->id;

        return array(
            'select' => array('select' => 'id, name', 'condition' => $condition),
            'self' => array('select' => 'id, user_id, name', 'condition' => $condition),
            'countSelf' => array('select' => 'COUNT(id) as id', 'condition' => $condition)
        );
    }

    public function beforeSave()
    {
        $this->user_id = Yii::app()->user->id;

        return true;
    }

    public static function  getDropDown()
    {
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM categories');
        $model = self::model()->cache(self::$duration, $dep, 1)->select()->findAll();

        return CHtml::listData($model, 'id', 'name');
    }
}