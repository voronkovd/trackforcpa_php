<?php

class Users extends ActiveRecord
{

    const STATUS_ENABLE = 1;

    const STATUS_DISABLE = 0;

    public $statuses = array(0 => 'Disable', 1 => 'Enable');

    public function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return array(
            array('email, password, last_login', 'required'),
            array('role, status', 'numerical', 'integerOnly' => true),
            array('email, password', 'length', 'max' => 32),
            array('id, email, password, role, last_login, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'categories' => array(self::HAS_MANY, 'Categories', 'user_id'),
            'clicks' => array(self::HAS_MANY, 'Clicks', 'user_id'),
            'links' => array(self::HAS_MANY, 'Links', 'user_id'),
            'refers' => array(self::HAS_MANY, 'Refers', 'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
            'password' => 'Password',
            'last_login' => 'Last Login',
            'status' => 'Status',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}