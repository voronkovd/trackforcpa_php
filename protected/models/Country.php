<?php

class Country extends ActiveRecord
{
    public function tableName()
    {
        return 'django_geoip_country';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array(
            'iprange' => array(self::HAS_MANY, 'Iprange', 'country_id'),
            'region' => array(self::HAS_MANY, 'Region', 'country_id'),
        );
    }

    public function attributeLabels()
    {
        return array();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}