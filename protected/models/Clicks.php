<?php

class Clicks extends ActiveRecord
{

    public $devices = array(1 => 'Компьютер', 2 => 'Телефон', 3 => 'Планшет');
    public $booleans = array(1 => 'Да');

    public function tableName()
    {
        return 'clicks';
    }

    public function rules()
    {
        return array(
            array(
                'id, rule_id, link_id, refer_id, user_id, geo_ip_ip_range_id, ip,
                created, order, browser_id, os_id, device_type, is_bot', 'safe', 'on' => 'search'
            )
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'browser' => array(self::BELONGS_TO, 'Browsers', 'browser_id'),
            'os' => array(self::BELONGS_TO, 'Oses', 'os_id'),
            'rule' => array(self::BELONGS_TO, 'RulesUrl', 'rule_id'),
            'link' => array(self::BELONGS_TO, 'Links', 'link_id'),
            'refer' => array(self::BELONGS_TO, 'Refers', 'refer_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'rule_id' => 'Правило',
            'link_id' => 'Ссылка',
            'refer_id' => 'Рефер',
            'geo_ip_ip_range_id' => 'Местоположения',
            'ip' => 'IP адрес',
            'created' => 'Дата и время',
            'order' => 'Заказ',
            'browser_id' => 'Браузер',
            'os_id' => 'ОС',
            'device_type' => 'Устройство',
            'is_bot' => 'Бот',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->compare('t.rule_id', $this->rule_id);
        $criteria->compare('t.link_id', $this->link_id);
        $criteria->compare('t.refer_id', $this->refer_id);
        $criteria->compare('t.ip', $this->ip, true);
        $criteria->compare('t.order', $this->order);
        $criteria->compare('t.is_bot', $this->is_bot);
        $criteria->compare('t.browser_id', $this->browser_id);
        $criteria->compare('t.os_id', $this->os_id);
        $criteria->compare('t.device_type', $this->device_type);
        $criteria->compare('t.user_id', Yii::app()->user->id);
        $criteria->compare('DATE(t.created)', $this->created);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => $this->pagination));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        return array('public' => array('select' => '*', 'condition' => array()));
    }

    public function getDevice()
    {
        if (array_key_exists($this->device_type, $this->devices)) {
            return $this->devices[$this->device_type];
        } else {
            return '';
        }
    }

    public function getIp()
    {
        if ($this->ip) {
            $url = Yii::app()->params['whois'] . $this->ip;

            return CHtml::link($this->ip, $url, array('target' => '_blank'));
        } else {
            return '';
        }
    }

    public function getBoolean($data)
    {
        if ($data == 1) {
            $class = 'glyphicon glyphicon-ok';
        } else {
            $class = 'glyphicon glyphicon-remove';
        }

        return CHtml::tag('span', array('class' => $class));
    }

    public function geo()
    {
        $result = '';
        if ($this->geo_ip_ip_range_id != 0) {
            $ip = Iprange::model()->findByPk($this->geo_ip_ip_range_id);
            if ($ip != null) {
                $result = $ip->country->name;
            }

        }

        return $result;
    }
}