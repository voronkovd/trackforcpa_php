<?php

class Links extends ActiveRecord
{
    public $rulesOptions
        = array(
            'name' => array(
                'maxlength' => 32,
                'autocomplete' => 'off',
                'autofocus' => 'autofocus',
                'placeholder' => 'Например: Товар из сельхоз магазина'

            ),
            'url' => array(
                'maxlength' => 253,
                'autocomplete' => 'on',
                'placeholder' => 'Например: http://example.com/'
            ),
        );

    public function tableName()
    {
        return 'links';
    }

    public function rules()
    {
        return array(
            array('name, url', 'required', 'message' => 'Поле не может быть пустым'),
            array('name', 'length', 'min' => 2, 'tooShort' => 'Минимальная длина 2 символа'),
            array('name', 'length', 'max' => 32, 'tooLong' => 'Максимальная длина 32 символа'),
            array('url', 'length', 'min' => 5, 'tooShort' => 'Минимальная длина 5 символов'),
            array('url', 'length', 'max' => 253, 'tooLong' => 'Максимальная длина 253 символа'),
            array('url', 'url', 'message' => 'Поле должно содержать ссылку'),
            array('id, name, url', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'clicks' => array(self::HAS_MANY, 'Clicks', 'rule_id'),
            'events' => array(self::HAS_MANY, 'Events', 'rule_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rules' => array(self::HAS_MANY, 'RulesUrl', 'link_id'),
        );
    }

    public function attributeLabels()
    {
        return array('name' => 'Наименование', 'url' => 'Ссылка');
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, t.name, t.url';
        $criteria->compare('user_id', Yii::app()->user->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.url', $this->url, true);
        $criteria->order = 't.id DESC';
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM links');
        $model = self::model()->cache(self::$duration, $dep, 1);

        return new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => $this->pagination));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        $condition = 'user_id=' . Yii::app()->user->id;

        return array(
            'select' => array('select' => 'id, name', 'condition' => $condition),
            'self' => array('select' => 'id, user_id, name, url', 'condition' => $condition),
            'countSelf' => array('select' => 'COUNT(id) as id', 'condition' => $condition)
        );
    }

    public function beforeDelete()
    {

        $criteria = new CDbCriteria();
        $criteria->compare('link_id', $this->id);
        Clicks::model()->deleteAll($criteria);
        Events::model()->deleteAll($criteria);
        RulesUrl::model()->deleteAll($criteria);

        return true;
    }

    public function beforeSave()
    {
        $this->user_id = Yii::app()->user->id;

        return true;
    }

    public static function  getDropDown()
    {
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM links');
        $model = self::model()->cache(self::$duration, $dep, 1)->select()->findAll();

        return CHtml::listData($model, 'id', 'name');
    }

    public function urlEncode()
    {
        return CHtml::link($this->url, $this->url, array('target' => '_blank'));
    }
}