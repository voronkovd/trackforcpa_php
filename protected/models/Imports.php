<?php

class Imports extends ActiveRecord
{
    public function tableName()
    {
        return 'imports';
    }

    public function rules()
    {
        return array(
            array('ip, created', 'required'),
            array('link_id, user_id', 'numerical', 'integerOnly' => true),
            array('ip', 'length', 'max' => 15),
            array('id, link_id, user_id, ip, created', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'link' => array(self::BELONGS_TO, 'Links', 'link_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'link_id' => 'Ссылка',
            'ip' => 'IP адрес',
            'created' => 'Дата и время',
        );
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function beforeSave()
    {
        return true;
    }
}