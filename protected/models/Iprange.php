<?php

class Iprange extends CActiveRecord
{
    public function tableName()
    {
        return 'django_geoip_iprange';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array(
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
        );
    }

    public function attributeLabels()
    {
        return array();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}