<?php

class ActiveRecord extends CActiveRecord
{
    public $rulesOptions = array();

    public static $duration = 1200;

    public $htmlOptions
        = array(
            'label' => array(
                'class' => 'control-label'
            ),
            'input' => array(
                'class' => 'form-control'
            ),

            'submitButton' => array(
                'class' => 'btn btn-success btn-sm'
            ),

            'cancelButton' => array(
                'class' => 'btn btn-danger btn-sm'
            ),

        );

    public $pagination = array('pageSize' => 12);

    public function getAction()
    {
        if ($this->isNewRecord) {
            return 'Добавить';
        } else {
            return 'Редактировать';
        }
    }

    public function mergeOptions($html = '', $rule = '')
    {
        $htmlOptions = array();
        $rulesOptions = array();
        if (array_key_exists($html, $this->htmlOptions)) {
            $htmlOptions = $this->htmlOptions[$html];
        }

        if (array_key_exists($rule, $this->rulesOptions)) {
            $rulesOptions = $this->rulesOptions[$rule];
        }

        return array_merge($htmlOptions, $rulesOptions);

    }

    public function deleteAll($condition = '', $params = array())
    {
        Yii::trace(get_class($this) . '.deleteAll()', 'system.db.ar.CActiveRecord');
        $builder = $this->getCommandBuilder();
        $criteria = $builder->createCriteria($condition, $params);
        $command = $builder->createDeleteCommand($this->getTableSchema(), $criteria);

        return $command->execute();
    }
}