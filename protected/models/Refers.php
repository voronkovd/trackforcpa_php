<?php

class Refers extends ActiveRecord
{
    public function tableName()
    {
        return 'refers';
    }

    public function rules()
    {
        return array(
            array('url', 'required'),
            array('user_id', 'numerical', 'integerOnly' => true),
            array('url', 'length', 'max' => 253),
            array('id, user_id, url', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'clicks' => array(self::HAS_MANY, 'Clicks', 'refer_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array('url' => 'Ссылка');
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id, t.url';
        $criteria->compare('t.user_id', Yii::app()->user->id);
        $criteria->compare('t.url', $this->url, true);
        $criteria->order = 't.id DESC';
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM refers');
        $model = self::model()->cache(self::$duration, $dep, 1);

        return new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => $this->pagination));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        $condition = 'user_id=' . Yii::app()->user->id;

        return array(
            'select' => array('select' => 'id, url', 'condition' => $condition),
            'self' => array('select' => 'id, url', 'condition' => $condition),
            'countSelf' => array('select' => 'COUNT(id) as id', 'condition' => $condition)
        );
    }

    public function WhoIs()
    {
        if ($this->url) {
            $url = Yii::app()->params['whois'] . $this->url;

            return CHtml::link($this->url, $url, array('target' => '_blank'));
        } else {
            return '';
        }
    }

    public static function  getDropDown()
    {
        $dep = new CDbCacheDependency('SELECT MAX(id) FROM refers');
        $model = self::model()->cache(self::$duration, $dep, 1)->select()->findAll();

        return CHtml::listData($model, 'id', 'url');
    }
}