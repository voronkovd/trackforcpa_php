<?php

class Statistics
{
    public $models
        = array(
            'Колличество категорий' => 'Categories',
            'Колличество ссылок' => 'Links',
            'Колличество правил' => 'RulesUrl',
            'Колличество реферов' => 'Refers',
        );

    public function getDashboardData()
    {
        $data = array();
        foreach ($this->models as $key => $model) {
            $cache = Yii::app()->cache->get($this->getHashName($model));
            if ($cache === false) {
                $cache = $this->countModel($model);
            }
            $data[$key] = $cache;
        }

        return $data;

    }

    protected function countModel($model)
    {
        $object = $model::model()->countSelf()->find();
        if (is_object($object)) {
            $count = $object->id;
        } else {
            $count = 0;
        }
        Yii::app()->cache->set($this->getHashName($model), $count, 180);

        return $count;
    }

    protected function  getHashName($model = '')
    {
        return Hash::work()->generate(Yii::app()->user->id . 'count' . $model);
    }

}