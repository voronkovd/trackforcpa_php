<div class="card card-signin">
    <h2 class="form-signin-heading text-center"><?php echo $model->header; ?></h2>
    <?php $form = $this->beginWidget('CActiveForm', $model->htmlOptions['form']); ?>
    <?php echo $form->textField($model, 'email', $model->htmlOptions['email']); ?>
    <?php echo $form->passwordField($model, 'password', $model->htmlOptions['password']); ?>
    <?php if (!empty($model->errors)): ?>
        <span class="text-danger"> <?php echo $form->error($model, 'email'); ?></span>
        <span class="text-danger"> <?php echo $form->error($model, 'password'); ?></span>
        <br/>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-4">
            <?php echo CHtml::submitButton('Войти', array('class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-8 text-right">
            <a href="#">Регистрация</a><br/>
            <a href="#">Забыли пароль?</a>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>