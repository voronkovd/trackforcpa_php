<div class="row">
    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="alert-user">
            <div class="alert alert-danger alert-dismissable">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-xs-4"></div>
    <div class="col-xs-4">
        <h3><?php echo $action = 'Смена пароля'; ?></h3>

        <div class="clearfix"></div>
        <?php $form = $this->beginWidget(
            'CActiveForm', array('id' => 'change-password-form', 'htmlOptions' => array('role' => 'form'))
        ); ?>
        <div class="form-group <?php echo $form->error($model, 'old_password') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'old_password', array('class' => 'control-label')); ?>
            <?php echo $form->PasswordField($model, 'old_password', array('class' => 'form-control')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'old_password'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'new_password') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'new_password', array('class' => 'control-label')); ?>
            <?php echo $form->PasswordField($model, 'new_password', array('class' => 'form-control')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'new_password'); ?></span>
        </div>
        <hr/>
        <div class="form-group btn-group pull-right">
            <?php echo CHtml::link(
                'Отмена', Yii::app()->createUrl('admin'), array('class' => 'btn btn-danger')
            ); ?>
            <?php echo CHtml::submitButton($action, array('class' => 'btn btn-success')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="col-xs-4"></div>
</div>