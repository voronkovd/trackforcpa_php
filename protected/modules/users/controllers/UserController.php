<?php

class UserController extends Controller
{

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('ChangePassword'), 'users' => array('@')),
            array('deny', 'users' => array('*')),
        );
    }

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function actionChangePassword()
    {
        $model = new ChangePasswordForm();
        if (isset($_POST['ChangePasswordForm'])) {
            $model->attributes = $_POST['ChangePasswordForm'];
            if ($model->validate() && $model->savePassword()) {
                Yii::app()->user->setFlash('change', 'Пароль успешно изменен!');
                $this->redirect(Yii::app()->user->returnUrl);
            } else {
                Yii::app()->user->setFlash('error', 'Не удалось изменить пароль');
            }
        }
        $this->title = 'Изменить пароль';
        $this->render('update', array('model' => $model));
    }

}