<?php

class AuthController extends Controller
{
    public $layout = '//layouts/auth';

    public function actionLogin()
    {
        if (!empty(Yii::app()->user->id)) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $model = new AuthForm();
        if (isset($_POST['AuthForm'])) {
            $model->attributes = $_POST['AuthForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        $this->title = 'Авторизация';
        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}