<?php

class UsersModule extends CWebModule
{

    public function beforeControllerAction($controller, $action)
    {
        $this->setImport(array('users.models.*', 'users.components.*'));
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else {
            return false;
        }
    }
}