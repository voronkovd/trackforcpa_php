<?php

class AuthForm extends FormModel
{
    private $_identity;

    public $email;

    public $password;

    public $rulesOptions = array();

    public $action = '';

    public $header = 'Авторизация';

    public $htmlOptions
        = array(
            'form' => array(
                'id' => 'login',
                'htmlOptions' => array(
                    'class' => 'form-signin',
                    'autocomplete' => 'off'
                )
            ),
            'email' => array(
                'class' => 'form-control',
                'placeholder' => 'Email:',
                'autofocus' => 'autofocus'
            ),
            'password' => array(
                'class' => 'form-control',
                'placeholder' => 'Пароль:'
            ),

            'submitButton' => array(
                'class' => 'btn btn-success btn-sm'
            ),

            'cancelButton' => array(
                'class' => 'btn btn-danger btn-sm'
            ),

        );

    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
            'password' => 'Пароль',
        );
    }

    public function rules()
    {
        return array(
            array('email, password', 'required'),
            array('email', 'email'),
            array('password', 'authenticate'),
        );
    }

    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', 'Неверный логин или пароль');
            }
        }
    }


    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = 3600 * 24 * 1; // 1 day
            Yii::app()->user->login($this->_identity, $duration);

            return true;
        } else {
            return false;
        }
    }
}