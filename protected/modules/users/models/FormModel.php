<?php

class FormModel extends CFormModel
{
    public $rulesOptions = array();

    public $action = '';

    public $header = '';

    public $htmlOptions = array();

    public function mergeOptions($html = '', $rule = '')
    {
        $htmlOptions = array();
        $rulesOptions = array();
        if (array_key_exists($html, $this->htmlOptions)) {
            $htmlOptions = $this->htmlOptions[$html];
        }

        if (array_key_exists($rule, $this->rulesOptions)) {
            $rulesOptions = $this->rulesOptions[$rule];
        }

        return array_merge($htmlOptions, $rulesOptions);

    }

}