<?php

class RegistrationForm extends FormModel
{

    public $email;

    public $password;

    public $passwordConfirm;

    public $rulesOptions = array();

    public $action = '';

    public $header = 'Регистрация';

    public $htmlOptions
        = array(
            'form' => array(
                'id' => 'registration',
                'htmlOptions' => array(
                    'class' => 'form-signin',
                    'autocomplete' => 'off'
                )
            ),
            'email' => array(
                'class' => 'form-control',
                'placeholder' => 'Email:',
                'autofocus' => 'autofocus'
            ),
            'password' => array(
                'class' => 'form-control',
                'placeholder' => 'Пароль:'
            ),

            'submitButton' => array(
                'class' => 'btn btn-success btn-sm'
            ),

            'cancelButton' => array(
                'class' => 'btn btn-danger btn-sm'
            ),

        );

    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
            'password' => 'Пароль',
            'passwordConfirm' => 'Пароль еше раз'
        );
    }

    public function rules()
    {
        return array(
            array('email, password', 'required'),
            array('email', 'email'),
            array('email', 'existInBase'),
            array('password', 'compare', 'compareAttribute' => 'passwordConfirm'),
        );
    }

    public function existInBase($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', 'Неверный логин или пароль');
            }
        }
    }


    public function create()
    {

    }

    public function sendEmail($email)
    {

    }
}