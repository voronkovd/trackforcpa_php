<?php

class ChangePasswordForm extends CFormModel
{
    public $old_password;
    public $new_password;

    private $_user = null;

    public function attributeLabels()
    {
        return array('old_password' => 'Текущий пароль', 'new_password' => 'Новый Пароль');
    }

    public function rules()
    {
        return array(array('old_password, new_password', 'required'), array('old_password', 'exists'));
    }

    public function exists($attribute, $params)
    {
        $this->_user = Users::model()->findByPk(Yii::app()->user->id);
        if ($this->hashPassword($this->old_password) != $this->_user->password) {
            $this->addError('old_password', 'Текущий пароль не верен');
        }
    }

    protected function hashPassword($password)
    {
        return md5(Yii::app()->params['secret_key'] . $password);
    }

    public function savePassword()
    {
        $this->_user->password = $this->hashPassword($this->new_password);
        $this->_user->save();

        return true;
    }
}
