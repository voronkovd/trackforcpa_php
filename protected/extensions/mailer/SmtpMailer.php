<?php

require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Mailer.php';

class SmtpMailer extends Mailer
{

    public $server;
    public $port = 25;
    public $timeout = 3;
    public $hello = 'EHLO';
    public $username;
    public $password;
    public $code;
    public $return;
    private $_fp;

    public function init()
    {
        parent::init();

        $this->_fp = fsockopen($this->server, $this->port, $errno, $errstr, $this->timeout);
        if (!$this->_fp) {
            throw new CException('Connect to smtp server failed: ' . $errstr);
        }

        $this->hello();
        $this->authenticate();
    }

    public function send($to, $subject, $message)
    {
        $to = is_array($to) ? $to : array($to);
        $message = str_replace("\r\n", "\n", $message);

        $code = $this->put("MAIL FROM:<{$this->username}>");
        if ($code != 250 && $code != 235) {
            throw new CException($this->return);
        }

        foreach ($to as $email) {
            $code = $this->put("RCPT TO:<{$email}>");
            if ($code != 250) {
                throw new CException($this->return);
            }
        }

        $code = $this->put("DATA");
        if ($code != 334 && $code != 250) {
            throw new CException($this->return);
        }

        $output = '';
        foreach ($to as $email) {
            $output .= "To: {$email}{$this->crlf}";
        }
        $output .= "Date: " . gmdate('r') . $this->crlf;
        $output .= "From: {$this->username}{$this->crlf}";
        $output .= "Subject: {$subject}{$this->crlf}";
        foreach ($this->headers as $header) {
            $output .= $header . $this->crlf;
        }
        $output .= $this->crlf . $this->crlf;
        $output .= $message;
        $output .= $this->crlf . ".";
        $code = $this->put($output);

        fclose($this->_fp);
        if ($code != 250) {
            return false;
        }

        return true;
    }

    protected function hello()
    {
        $auth = strtoupper($this->hello) == 'EHLO' ? 'EHLO' : 'HELO';
        $code = $this->put("{$auth} {$_SERVER['HTTP_HOST']}");
        if ($code != 220) {
            throw new CException($this->return);
        }
    }

    protected function authenticate()
    {
        $code = $this->put("AUTH LOGIN");
        if ($code != 250) {
            throw new CException($data);
        }

        $code = $this->put(base64_encode($this->username));
        if ($code != 250 && $code != 334) {
            throw new CException($this->return);
        }

        $code = $this->put(base64_encode($this->password));
        if ($code != 250 && $code != 334) {
            throw new CException($this->return);
        }
    }

    protected function put($cmd)
    {
        fputs($this->_fp, $cmd . $this->crlf);

        $this->return = '';
        while ($line = fgets($this->_fp, 128)) {
            $this->return .= $line;
            if (trim(substr($line, 3, 1)) == '') {
                break;
            }
        }
        $this->code = substr($this->return, 0, 3);

        return $this->code;
    }
}