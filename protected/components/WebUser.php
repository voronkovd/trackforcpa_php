<?php

class WebUser extends CWebUser
{

    private $_model = null;

    function getLogin()
    {
        if (!$this->isGuest) {
            $user = Users::model()->findByPk($this->id, array('select' => 'email'));

            return $user->login;
        } else {
            return null;
        }
    }

    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = Users::model()->findByPk($this->id, array('select' => 'id'));
        }

        return $this->_model;
    }

    public function logout($destroySession = true)
    {
        if ($this->allowAutoLogin && isset($this->identityCookie['domain'])) {
            $cookies = Yii::app()->getRequest()->getCookies();

            if (null !== ($cookie = $cookies[$this->getStateKeyPrefix()])) {
                $originalCookie = new CHttpCookie($cookie->name, $cookie->value);
                $cookie->domain = $this->identityCookie['domain'];
                $cookies->remove($this->getStateKeyPrefix());
                $cookies->add($originalCookie->name, $originalCookie);
            }
        }

        parent::logout($destroySession);
    }
}