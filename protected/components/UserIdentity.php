<?php

class UserIdentity extends CUserIdentity
{
    protected $_id;

    public function authenticate()
    {
        $user = Users::model()->find('LOWER(email)=?', array(strtolower($this->username)));
        if (($user === null) or (Hash::work()->generate($this->password) != $user->password)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $user->last_login = date('Y-m-d H:i:s');
            $user->save(false);
            $this->_id = $user->id;
            $this->username = $user->email;
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

}