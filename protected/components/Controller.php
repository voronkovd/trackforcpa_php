<?php

class Controller extends CController
{

    public $layout = '//layouts/main';

    public $menu
        = array(
            array(
                'label' => 'Панель управления',
                'url' => array('/site')
            ),
            array(
                'label' => 'Категории',
                'url' => array('/categories')
            ),
            array(
                'label' => 'Ссылки',
                'url' => array('/links')
            ),
            array(
                'label' => 'Правила',
                'url' => array('/rules')
            ),
            array(
                'label' => 'Реферы',
                'url' => array('/refers')
            ),
            array(
                'label' => 'Переходы',
                'url' => array('/clicks')
            ),
            array(
                'label' => 'Импорт',
                'url' => array('/imports')
            ),
//            array(
//                'label' => 'Статистика :',
//                'url' => array('#'),
//                'linkOptions' => array('class' => 'disable-url'),
//                'submenuOptions' => array('class' => 'nav'),
//                'items' => array(
//                    array(
//                        'label' => '- по переходам',
//                        'url' => array('/stat/clicks')
//                    ),
//                    array(
//                        'label' => '- по реферам',
//                        'url' => array('/stat/refers')
//                    ),
//                    array(
//                        'label' => '- по ссылкам',
//                        'url' => array('/stat/links')
//                    ),
//                    array(
//                        'label' => '- по правилам',
//                        'url' => array('/stat/rules')
//                    ),
//                ),
//            ),
            array(
                'label' => 'Профиль :',
                'url' => array('#'),
                'linkOptions' => array('class' => 'disable-url'),
                'submenuOptions' => array('class' => 'nav'),
                'items' => array(
                    array(
                        'label' => '- сменить пароль',
                        'url' => array('/change/password')
                    ),
                    array(
                        'label' => 'Выход',
                        'url' => array('/logout')
                    ),
                ),
            ),

        );

    public $breadcrumbs = array();

    public $alert = null;

    public $title = '';

    public function beforeAction($action)
    {
        Yii::app()->clientscript
            ->registerCssFile(Yii::app()->baseUrl . '/static/css/bootstrap.min.css')
            ->registerCssFile(Yii::app()->baseUrl . '/static/css/theme.css')
            ->registerCoreScript('jquery')
            ->registerCoreScript('jquery.ui')
            ->registerScriptFile(Yii::app()->baseUrl . '/static/js/jquery.cookie.js', CClientScript::POS_END)
            ->registerScriptFile(Yii::app()->baseUrl . '/static/js/bootstrap.min.js', CClientScript::POS_END)
            ->registerScriptFile(Yii::app()->baseUrl . '/static/js/moment.min.js', CClientScript::POS_END)
            ->registerScriptFile(Yii::app()->baseUrl . '/static/js/default.js', CClientScript::POS_END);

        return true;
    }
}