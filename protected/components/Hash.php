<?php

class Hash
{
    protected static $_instance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function generate($string = '')
    {
        $result = Yii::app()->params['secret_key'];
        if (is_string($string)) {
            $result .= $string;
        }

        return md5($result);
    }

    public static function work()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

}