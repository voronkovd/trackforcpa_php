<?php

require_once dirname(__FILE__) . '/../vendors/redis/Rediska.php';

class RedisConnection
{
    public $options = array();

    private $_rediska;


    public function init()
    {
        $this->_rediska = new Rediska($this->options);
    }


    public function getConnection()
    {
        return $this->_rediska;
    }
}