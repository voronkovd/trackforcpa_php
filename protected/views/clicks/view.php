<h4>View Clicks #<?php echo $model->id; ?></h4>

<?php $this->widget(
    'zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'rule_id',
            'link_id',
            'refer_id',
            'user_id',
            'geo_ip_ip_range_id',
            'ip',
            'os',
            'browser',
            'created',
            'order',
        ),
    )
);