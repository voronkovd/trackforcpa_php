<p class="btn help-block ">Необходимо сделать импорт с вашей CPA системы, чтобы в графе заказ, появились данные</p>
<?php


$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'clicks-grid',
        'dataProvider' => $model->search(),
        'enableSorting' => false,
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'ip',
                'value' => '$data->getIp()',
                'type' => 'raw',
                'filter' => CHtml::activeTextField($model, 'ip', array('placeholder' => ' Поиск...')),
                'htmlOptions' => array('style' => 'width:10%'),
            ),
            array(
                'name' => 'link_id',
                'value' => '$data->link->name',
                'filter' => Links::getDropDown(),
                'htmlOptions' => array('style' => 'width:10%'),
            ),
            array(
                'name' => 'refer_id',
                'value' => '$data->refer->url',
                'filter' => Refers::getDropDown(),
            ),
            array(
                'name' => 'rule_id',
                'filter' => RulesUrl::getDropDown(),
                'value' => '$data->rule->name',
                'htmlOptions' => array('style' => 'width:10%'),
            ),
            array(
                'name' => 'os_id',
                'value' => '$data->os->name',
                'filter' => Oses::getDropDown(),
                'htmlOptions' => array('style' => 'width:8%'),
            ),
            array(
                'name' => 'browser_id',
                'value' => '$data->browser->name',
                'filter' => Browsers::getDropDown(),
                'htmlOptions' => array('style' => 'width:8%'),
            ),
            array(
                'name' => 'device_type',
                'value' => '$data->getDevice()',
                'filter' => $model->devices,
                'htmlOptions' => array('style' => 'width:7%'),
            ),
            array(
                'name' => 'order',
                'value' => '$data->getBoolean($data->order)',
                'filter' => $model->booleans,
                'type' => 'raw',
                'htmlOptions' => array('style' => 'width:7%', 'class' => 'text-center'),
            ),
            array(
                'name' => 'is_bot',
                'value' => '$data->getBoolean($data->is_bot)',
                'filter' => $model->booleans,
                'type' => 'raw',
                'htmlOptions' => array('style' => 'width:5%', 'class' => 'text-center'),
            ),
            array(
                'name' => 'created',
                'filter' => $this->widget(
                        'zii.widgets.jui.CJuiDatePicker',
                        array('model' => $model, 'attribute' => 'created'), true
                    ),
                'htmlOptions' => array('style' => 'width:12%'),
            ),
            array(
                'name' => 'geo_ip_ip_range_id',
                'value' => '$data->geo()',
                'filter' => false,
                'htmlOptions' => array('style' => 'width:10%'),
            ),
        ),
    )
);