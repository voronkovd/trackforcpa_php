<div class="row">
    <div class="col-xs-4">
        <h3><?php echo $action = $model->isNewRecord ? 'Добавить' : 'Редактировать'; ?></h3>

        <div class="clearfix"></div>
        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'events')); ?>
        <div class="form-group <?php echo $form->error($model, 'link_id') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'link_id', array('class' => 'control-label')); ?>
            <?php echo $form->textField($model, 'link_id', array('class' => 'form-control')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'link_id'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'type') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'type', array('class' => 'control-label')); ?>
            <?php echo $form->textField($model, 'type', array('class' => 'form-control')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'type'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'value') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'value', array('class' => 'control-label')); ?>
            <?php echo $form->textField($model, 'value', array('class' => 'form-control')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'value'); ?></span>
        </div>
        <hr/>
        <p class="help-block"><small>Поля с * - обязательные для заполнения.</small></p>
        <div class="form-group btn-group pull-right">
            <?php echo CHtml::link('Отмена', Yii::app()->createUrl('events'), array('class' => 'btn btn-danger')); ?>
            <?php echo CHtml::submitButton($action, array('class' => 'btn btn-success')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>