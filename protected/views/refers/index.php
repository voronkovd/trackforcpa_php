<?php
$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'refers-grid',
        'dataProvider' => $model->search(),
        'enableSorting' => false,
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'url',
                'value' => '$data->whoIs()',
                'type' => 'raw',
                'filter' => CHtml::activeTextField($model, 'url', array('placeholder' => ' Поиск по ссылке...'))
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '<div class="btn-group">{view}</div>',
                'htmlOptions' => array('class' => 'text-center', 'style' => 'width:120px;'),
                'header' => 'Действия',
                'viewButtonImageUrl' => false,
                'viewButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Просмотр'
                ),
                'buttons' => array(
                    'view' => array('label' => '<span class="glyphicon glyphicon-eye-open"></span>')
                ),
            ),
        ),
    )
);