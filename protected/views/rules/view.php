<?php

$this->widget(
    'zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            array('name' => 'name'),
            array('name' => 'category_id', 'value' => $model->category->name),
            array('name' => 'link_id', 'value' => $model->link->name),
            array('name' => 'token', 'value' => $model->urlEncode(), 'type' => 'raw'),
            array('name' => 'status', 'value' => $model->getStatus(), 'type' => 'raw'),
        ),
    )
);

$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'clicks-grid',
        'dataProvider' => $clicks->search(),
        'enableSorting' => false,
        'filter' => $clicks,
        'columns' => array(
            array(
                'name' => 'ip',
                'value' => '$data->getIp()',
                'type' => 'raw',
                'filter' => CHtml::activeTextField($clicks, 'ip', array('placeholder' => ' Поиск...')),
                'htmlOptions' => array('style' => 'width:10%'),
            ),
            array(
                'name' => 'link_id',
                'value' => '$data->link->name',
                'filter' => Links::getDropDown(),
                'htmlOptions' => array('style' => 'width:10%'),
            ),
            array(
                'name' => 'refer_id',
                'filter' => Refers::getDropDown(),
                'value' => '$data->refer->url',
            ),
            array(
                'name' => 'os_id',
                'value' => '$data->os->name',
                'filter' => Oses::getDropDown(),
                'htmlOptions' => array('style' => 'width:8%'),
            ),
            array(
                'name' => 'browser_id',
                'value' => '$data->browser->name',
                'filter' => Browsers::getDropDown(),
                'htmlOptions' => array('style' => 'width:8%'),
            ),
            array(
                'name' => 'device_type',
                'value' => '$data->getDevice()',
                'filter' => $clicks->devices,
                'htmlOptions' => array('style' => 'width:7%'),
            ),
            array(
                'name' => 'order',
                'value' => '$data->getBoolean($data->order)',
                'filter' => $clicks->booleans,
                'type' => 'raw',
                'htmlOptions' => array('style' => 'width:7%', 'class' => 'text-center'),
            ),
            array(
                'name' => 'is_bot',
                'value' => '$data->getBoolean($data->is_bot)',
                'filter' => $clicks->booleans,
                'type' => 'raw',
                'htmlOptions' => array('style' => 'width:5%', 'class' => 'text-center'),
            ),
            array(
                'name' => 'created',
                'filter' => $this->widget(
                        'zii.widgets.jui.CJuiDatePicker',
                        array('model' => $clicks, 'attribute' => 'created'), true
                    ),
                'htmlOptions' => array('style' => 'width:12%'),
            ),
            array(
                'name' => 'geo_ip_ip_range_id',
                'value' => '$data->geo()',
                'filter' => false,
                'htmlOptions' => array('style' => 'width:10%'),
            ),
        ),
    )
);