<?php echo CHtml::link('Добавить', Yii::app()->createUrl('rules/create'), array('class' => 'btn btn-default btn-sm')); ?>
    <p class="btn help-block ">Внимание! Удаление правила, повлияет на статистику по переходам.</p>
<?php
$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'rules-grid',
        'dataProvider' => $model->search(),
        'enableSorting' => false,
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'name',
                'filter' => CHtml::activeTextField(
                        $model, 'name', array('placeholder' => ' Поиск по наименованию...')
                    )
            ),
            array('name' => 'category_id', 'value' => '$data->category->name', 'filter' => Categories::getDropDown()),
            array('name' => 'link_id', 'value' => '$data->link->name', 'filter' => Links::getDropDown()),
            array(
                'name' => 'status',
                'value' => '$data->getStatus()',
                'type' => 'raw',
                'filter' => $model::$statuses,
                'htmlOptions' => array('class' => 'text-center')
            ),
            array('name' => 'token', 'value' => '$data->urlEncode()', 'type' => 'raw', 'filter' => false),
            array(
                'class' => 'CButtonColumn',
                'template' => '<div class="btn-group">{view}{update}{delete}</div>',
                'htmlOptions' => array('class' => 'text-center', 'style' => 'width:120px;'),
                'header' => 'Действия',
                'updateButtonImageUrl' => false,
                'viewButtonImageUrl' => false,
                'deleteButtonImageUrl' => false,
                'deleteConfirmation' => 'Вы действительно хотите удалить правило?',
                'deleteButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default delete',
                    'title' => 'Удалить'
                ),
                'updateButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Редактировать'
                ),

                'viewButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Просмотр'
                ),
                'buttons' => array(
                    'update' => array('label' => '<span class="glyphicon glyphicon-pencil"></span>'),
                    'delete' => array('label' => '<span class="glyphicon glyphicon-trash"></span>'),
                    'view' => array('label' => '<span class="glyphicon glyphicon-eye-open"></span>')
                ),
            ),
        ),
    )
);