<div class="row">
    <div class="col-xs-5">
        <h3><?php echo $model->getAction(); ?></h3>
        <?php if (!empty($model->errors)): ?>
            <?php $this->renderPartial('//layouts/_alert_errors'); ?>
        <?php endif; ?>
        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'rules-form')); ?>
        <div class="form-group <?php echo $form->error($model, 'name') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'name', $model->htmlOptions['label']); ?>
            <?php echo $form->textField($model, 'name', $model->mergeOptions('input', 'name')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'name'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'category_id') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'category_id', $model->htmlOptions['label']); ?>
            <?php echo $form->dropDownList(
                $model, 'category_id', $model->categories, $model->mergeOptions('input', 'category_id')
            ); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'category_id'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'link_id') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'link_id', $model->htmlOptions['label']); ?>
            <?php echo $form->dropDownList(
                $model, 'link_id', $model->links, $model->mergeOptions('input', 'link_id')
            ); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'link_id'); ?></span>
        </div>
        <div class="checkbox">
            <label><?php echo $form->checkBox($model, 'status'); ?> В работе</label>
            <span class="text-danger"> <?php echo $form->error($model, 'status'); ?></span>
        </div>
        <hr/>
        <p class="help-block"><small>Поля с * - обязательные для заполнения.</small></p>
        <div class="form-group btn-group pull-right">
            <?php echo CHtml::link('Отмена', Yii::app()->createUrl('rules'), $model->htmlOptions['cancelButton']); ?>
            <?php echo CHtml::submitButton($model->getAction(), $model->htmlOptions['submitButton']); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>