<div class="error text-center">
    <h1>Ошибка <?php echo $code; ?></h1>
    <p class="lead"><?php echo CHtml::encode($message); ?></p>
</div>