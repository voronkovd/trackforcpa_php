<div class="row placeholders">
    <?php foreach ($data as $key => $value): ?>
        <div class="col-xs-6 col-sm-3 placeholder">
            <h1><?php echo $value; ?></h1>
            <span class="text-muted"><?php echo $key; ?></span>
        </div>
    <?php endforeach; ?>
</div>