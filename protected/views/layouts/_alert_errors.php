<div id="error-alert" class="alert alert-danger  alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i
            class="glyphicon glyphicon-remove"></i></button>
    <div class="text-center">
        Необходимо исправить следующие ошибки:
    </div>
</div>