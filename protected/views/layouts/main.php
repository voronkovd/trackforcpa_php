<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <title><?php echo $this->title; ?></title>
    <meta charset="utf-8">
    <link href="<?php echo Yii::app()->baseUrl; ?>/static/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
</head>
<body>
<div class="overlay" style="display: none;">
    <div>
        <img src="<?php echo Yii::app()->baseUrl; ?>/static/img/loading.gif"/>
    </div>
</div>
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <b class="navbar-brand">TRACK FOR CPA</b>
    <a title="Показать/Спрятать меню" class="navbar-brand show-hide-menu" href="#"><i class="glyphicon glyphicon-tasks"></i></a>
        <div class="collapse navbar-collapse pull-right">
            <div><h4 id="time" class="brand"></h4></div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php $this->widget('ext.MyMenu', array('items' => $this->menu)); ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php $this->widget('ext.MyBreadCrumbs', array('links' => $this->breadcrumbs)); ?>
            <?php if (!empty($this->breadcrumbs)): ?>
                <hr/>
            <?php endif; ?>
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>