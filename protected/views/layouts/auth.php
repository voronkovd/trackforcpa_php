<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <title><?php echo $this->title; ?></title>
    <meta charset="utf-8">
    <link href="<?php echo Yii::app()->baseUrl; ?>/static/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <?php Yii::app()->clientscript->registerCssFile(Yii::app()->baseUrl . '/static/css/theme-auth.css'); ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php echo $content; ?>
    </div>
</div>
</div>
</body>
</html>