<div class="row">
    <br />
    <br />
    <br />
    <div class="col-xs-4">
        <h3><?php echo $action = $model->isNewRecord ? 'Добавить' : 'Редактировать'; ?></h3>

        <div class="clearfix"></div>
        <?php $form = $this->beginWidget(
            'CActiveForm', array('id' => 'imports-form', 'htmlOptions' => array('role' => 'form'))
        ); ?>
        <div class="form-group <?php echo $form->error($model, 'ip') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'ip', array('class' => 'control-label')); ?>
            <?php echo $form->textField(
                $model, 'ip', array('size' => 15, 'maxlength' => 15, 'class' => 'form-control')
            ); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'ip'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'created') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'created', array('class' => 'control-label')); ?>
            <?php
            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker', array('model' => $model,
                    'attribute' => 'created', 'mode' => 'datetime',  'htmlOptions'=>array('class' => 'form-control'),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                    ),));
            ?>
            <span class="text-danger"> <?php echo $form->error($model, 'created'); ?></span>
        </div>
        <hr/>
        <p class="help-block">
            <small>Поля с * - обязательные для заполнения.</small>
        </p>
        <div class="form-group btn-group pull-right">


            <?php echo CHtml::link('Отмена', Yii::app()->createUrl('Imports'), array('class' => 'btn btn-danger')); ?>


            <?php echo CHtml::submitButton($action, array('class' => 'btn btn-success')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>