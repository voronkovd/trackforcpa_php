<?php echo CHtml::link('Добавить', Yii::app()->createUrl('categories/create'), array('class' => 'btn btn-default btn-sm')); ?>
<?php
$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'categories-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'enableSorting' => false,
        'columns' => array(
            array(
                'name' => 'name',
                'filter' => CHtml::activeTextField($model, 'name', array('placeholder' => ' Поиск по категории...'))
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '<div class="btn-group">{update}</div>',
                'htmlOptions' => array('style' => 'width:120px;', 'class' => 'text-center'),
                'header' => 'Действия',
                'updateButtonImageUrl' => false,
                'updateButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Редактировать'
                ),
                'buttons' => array(
                    'update' => array('label' => '<span class="glyphicon glyphicon-pencil"></span>'),
                ),
            ),
        ),
    )
);