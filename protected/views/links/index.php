<?php echo CHtml::link('Добавить', Yii::app()->createUrl('links/create'), array('class' => 'btn btn-default btn-sm')); ?>
    <p class="btn help-block ">Внимание! Удаление ссылки, повлияет на статистику по переходам.</p>
<?php
$this->widget(
    'zii.widgets.grid.CGridView', array(
        'id' => 'links-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'enableSorting' => false,
        'columns' => array(
            array(
                'name' => 'name',
                'filter' => CHtml::activeTextField($model, 'name', array('placeholder' => ' Поиск по Наименованию...'))
            ),
            array(
                'name' => 'url',
                'filter' => CHtml::activeTextField($model, 'url', array('placeholder' => ' Поиск по ссылке...')),
                'value' => '$data->urlEncode()',
                'type' => 'raw'
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '<div class="btn-group">{view}{update}{delete}</div>',
                'htmlOptions' => array('class' => 'text-center', 'style' => 'width:150px;'),
                'header' => 'Действия',
                'updateButtonImageUrl' => false,
                'viewButtonImageUrl' => false,
                'deleteButtonImageUrl' => false,
                'deleteConfirmation' => 'Вы действительно хотите удалить ссылку?',
                'deleteButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default delete',
                    'title' => 'Удалить'
                ),
                'updateButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Редактировать'
                ),

                'viewButtonOptions' => array(
                    'class' => 'btn btn-sm btn-default',
                    'title' => 'Просмотр'
                ),
                'buttons' => array(
                    'update' => array('label' => '<span class="glyphicon glyphicon-pencil"></span>'),
                    'delete' => array('label' => '<span class="glyphicon glyphicon-trash"></span>'),
                    'view' => array('label' => '<span class="glyphicon glyphicon-eye-open"></span>')
                ),
            ),
        ),
    )
);