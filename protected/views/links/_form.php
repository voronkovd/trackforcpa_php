<div class="row">
    <div class="col-xs-5">
        <h3><?php echo $model->getAction(); ?></h3>
        <?php if (!empty($model->errors)): ?>
            <?php $this->renderPartial('//layouts/_alert_errors'); ?>
        <?php endif; ?>
        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'links')); ?>
        <div class="form-group <?php echo $form->error($model, 'name') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'name', $model->htmlOptions['label']); ?>
            <?php echo $form->textField($model, 'name', $model->mergeOptions('input', 'name')); ?>
            <span class="text-danger"><?php echo $form->error($model, 'name'); ?></span>
        </div>
        <div class="form-group <?php echo $form->error($model, 'url') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'url', $model->htmlOptions['label']); ?>
            <?php echo $form->textField($model, 'url', $model->mergeOptions('input', 'url')); ?>
            <span class="text-danger"> <?php echo $form->error($model, 'url'); ?></span>
        </div>
        <hr/>
        <p class="help-block">
            <small>Поля с * - обязательные для заполнения.</small>
        </p>
        <div class="form-group btn-group pull-right">
            <?php echo CHtml::link('Отмена', Yii::app()->createUrl('links'), $model->htmlOptions['cancelButton']); ?>
            <?php echo CHtml::submitButton($model->getAction(), $model->htmlOptions['submitButton']); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>