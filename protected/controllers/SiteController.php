<?php

class SiteController extends Controller
{
    public $name = 'Панель управления';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('error'), 'users' => array('*')),
            array('allow', 'actions' => array('index'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    public function actionIndex()
    {
        $this->title = $this->name;
        $model = new Statistics();
        $data = $model->getDashboardData();
        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode($data);
            Yii::app()->end();
        }
        $this->render('index', array('data' => $data));
    }

    public function actionError()
    {
        if (Yii::app()->user->isGuest) {
            $this->layout = '//layouts/auth';
        }
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->title = 'Ошибка ' . $error['code'];
                $this->render('error', $error);
            }
        }
    }
}