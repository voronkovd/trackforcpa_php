<?php

class EventsController extends Controller
{

    public $name = 'Поведение';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('create', 'delete'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Events::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionCreate($id)
    {
        $this->title = $this->name . ' - [добавить]';
        $model = new Events;
        if (isset($_POST['Events'])) {
            $model->attributes = $_POST['Events'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/admin/EventsController'), 'Просмотр');
        $this->render('_form', array('model' => $model));
    }


    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }
}