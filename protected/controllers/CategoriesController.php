<?php

class CategoriesController extends Controller
{

    public $name = 'Категории';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index', 'create', 'update'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Categories::model()->self()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name;
        $model = new Categories('search');
        $model->unsetAttributes();
        if (isset($_GET['Categories'])) {
            $model->attributes = $_GET['Categories'];
        }
        $this->breadcrumbs = array($this->name);
        $this->render('index', array('model' => $model));
    }

    public function actionCreate()
    {
        $this->title = $this->name . ' - Добавить';
        $model = new Categories;
        if (isset($_POST['Categories'])) {
            $model->attributes = $_POST['Categories'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/categories'), 'Добавить');
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $this->title = $this->name . ' - редактировать';
        $model = $this->loadModel($id);
        if (isset($_POST['Categories'])) {
            $model->attributes = $_POST['Categories'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/categories'), 'Редактировать');
        $this->render('_form', array('model' => $model));
    }
}