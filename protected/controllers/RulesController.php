<?php

class RulesController extends Controller
{

    public $name = 'Правила';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index', 'view', 'create', 'update', 'delete'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = RulesUrl::model()->self()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name . ' - [администрирование]';
        $model = new RulesUrl('search');
        $model->unsetAttributes();
        if (isset($_GET['RulesUrl'])) {
            $model->attributes = $_GET['RulesUrl'];
        }
        $this->breadcrumbs = array($this->name);
        $this->render('index', array('model' => $model));
    }

    public function actionView($id)
    {
        $this->title = $this->name . ' - [просмотр]';
        $model = $this->loadModel($id);
        $clicks = new Clicks('search');
        $clicks->unsetAttributes();
        $clicks->rule_id = $id;
        if (isset($_GET['Clicks'])) {
            $clicks->attributes = $_GET['Clicks'];
        }
        $this->breadcrumbs = array($this->name => array('/rules'), 'Просмотр');
        $this->render('view', array('model' => $model, 'clicks' => $clicks));
    }

    public function actionCreate()
    {
        $this->title = $this->name . ' - [добавить]';
        $model = new RulesUrl;
        $model->categories = Categories::getDropDown();
        $model->links = Links::getDropDown();
        if (isset($_POST['RulesUrl'])) {
            $model->attributes = $_POST['RulesUrl'];
            if ($model->save()) {
                $model->token = Hash::work()->generate($model->id);
                $model->save();
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/rules'), 'Добавить');
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $this->title = $this->name . ' - [изменить]';
        $model = $this->loadModel($id);
        $model->categories = Categories::getDropDown();
        $model->links = Links::getDropDown();
        if (isset($_POST['RulesUrl'])) {
            $model->attributes = $_POST['RulesUrl'];
            if ($model->save()) {
                $model->token = Hash::work()->generate($model->id);
                $model->save();
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/rules'), 'Изменить');
        $this->render('_form', array('model' => $model));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }
}