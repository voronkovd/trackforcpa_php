<?php

class ImportsController extends Controller
{

    public $name = 'Импорт';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Imports::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name;
        $model = new Imports;
        if (isset($_POST['Imports'])) {
            $model->attributes = $_POST['Imports'];
            if ($model->save()) {
                Yii::app()->user->setFlash('change', 'Запись успешно добавлена!');
                $this->redirect(array('index'));
            } else {
                Yii::app()->user->setFlash('error', 'Не удалось добавить запись!');
            }
        }
        $this->breadcrumbs = array($this->name);
        $this->render('_form', array('model' => $model));
    }


}