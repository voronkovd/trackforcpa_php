<?php

class ClicksController extends Controller
{

    public $name = 'Переходы';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Categories::model()->self()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name;
        $model = new Clicks('search');
        $model->unsetAttributes();
        if (isset($_GET['Clicks'])) {
            $model->attributes = $_GET['Clicks'];
        }
        $this->breadcrumbs = array($this->name);
        $this->render('index', array('model' => $model));
    }

    public function actionView($id)
    {
        $this->title = $this->name . ' - [просмотр]';
        $model = $this->loadModel($id);
        $this->breadcrumbs = array($this->name => array('/refers'), 'Просмотр');
        $this->render('view', array('model' => $model));
    }
}