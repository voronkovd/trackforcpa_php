<?php

class LinksController extends Controller
{

    public $name = 'Ссылки';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index', 'create', 'update', 'delete', 'view'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Links::model()->self()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name;
        $model = new Links('search');
        $model->unsetAttributes();
        if (isset($_GET['Links'])) {
            $model->attributes = $_GET['Links'];
        }
        $this->breadcrumbs = array($this->name);
        $this->render('index', array('model' => $model));
    }

    public function actionCreate()
    {
        $this->title = $this->name . ' - Добавить';
        $model = new Links;
        if (isset($_POST['Links'])) {
            $model->attributes = $_POST['Links'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/links'), 'Добавить');
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $this->title = $this->name . ' - Редактировать';
        $model = $this->loadModel($id);
        if (isset($_POST['Links'])) {
            $model->attributes = $_POST['Links'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->breadcrumbs = array($this->name => array('/links'), 'Редактировать');
        $this->render('_form', array('model' => $model));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionView($id)
    {
        $this->title = $this->name . ' - Просмотр';
        $model = $this->loadModel($id);
        $clicks = new Clicks('search');
        $clicks->unsetAttributes();
        $clicks->link_id = $id;
        if (isset($_GET['Clicks'])) {
            $clicks->attributes = $_GET['Clicks'];
        }
        $this->breadcrumbs = array($this->name => array('/links'), 'Просмотр');
        $this->render('view', array('model' => $model, 'clicks' => $clicks));
    }
}