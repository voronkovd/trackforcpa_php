<?php

class RefersController extends Controller
{

    public $name = 'Реферы';

    public function filters()
    {
        return array('accessControl', 'postOnly + delete');
    }

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('index', 'view'), 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    protected function loadModel($id)
    {
        $model = Refers::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена.');
        }

        return $model;
    }

    public function actionIndex()
    {
        $this->title = $this->name . ' - [администрирование]';
        $model = new Refers('search');
        $model->unsetAttributes();
        if (isset($_GET['Refers'])) {
            $model->attributes = $_GET['Refers'];
        }
        $this->breadcrumbs = array($this->name);
        $this->render('index', array('model' => $model));
    }

    public function actionView($id)
    {
        $this->title = $this->name . ' - [просмотр]';
        $model = $this->loadModel($id);
        $clicks = new Clicks('search');
        $clicks->unsetAttributes();
        $clicks->refer_id = $id;
        if (isset($_GET['Clicks'])) {
            $clicks->attributes = $_GET['Clicks'];
        }
        $this->breadcrumbs = array($this->name => array('/refers'), 'Просмотр');
        $this->render('view', array('model' => $model, 'clicks' => $clicks));
    }

}